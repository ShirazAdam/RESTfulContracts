﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RESTfulSite.Startup))]
namespace RESTfulSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
