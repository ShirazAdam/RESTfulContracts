﻿using System.Threading.Tasks;
using System.Web.Mvc;
using RESTfulContract.Service;

namespace RESTfulSite.Controllers
{
    public class APITestController : Controller
    {
        public async Task<ActionResult> Index()
        {
            IRestfulSample service = new RestfulServiceSample();
            ViewBag.Sample = await service.GetSample(0);
            return View(ViewBag);
        }

        [HttpPost]
        public async Task<ActionResult> Index(int value)
        {
            IRestfulSample service = new RestfulServiceSample();
            ViewBag.Sample = await service.GetSample(value);
            return View(ViewBag);
        }
    }
}
