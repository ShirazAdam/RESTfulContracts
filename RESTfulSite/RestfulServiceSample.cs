﻿using System.Threading.Tasks;
using System.Web.Http;
using RESTfulContract.Service;
using RESTfulCore;

namespace RESTfulSite
{
    public class RestfulServiceSample : RestClientBase<IRestfulSample>, IRestfulSample
    {
        public async Task<string> GetSample(int id)
        {
            //call api route, first param is api method, second and beyond are all params that make up the query
            return await GetStringAsync("Default", "999");
        }

        public async Task<string> GetSampleUsingFluentInterface(int id)
        {
            var route = new RestRoute();

            route
                .CreateRequest
                .Route("Default/{value}")
                .SetParameter("{value}", id)
                .BuildRoute();

            var response = await GetAsync(route.RequestRoute);

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadAsStringAsync();

            throw new HttpResponseException(response.StatusCode);
        }
    }
}