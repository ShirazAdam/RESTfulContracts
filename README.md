# RESTfulContracts
Using abstraction for interfacing with RESTful services in a similar manner when consuming WCF services by using configuration files and service channel factory.

Updates are originally made on CodePlex (TFVC) source control at http://restfulcontracts.codeplex.com/ first and then copied across to this GitHub repository.
