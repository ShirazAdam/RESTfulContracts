using System.Collections.Generic;

namespace RESTfulCore
{
    public class RestRoute
    {
        public RestRouteFluentInterface CreateRequest { get; set; }

        public string OriginalRoute { get; set; }

        public string ActionToInvoke { get; set; }

        public string RequestRoute { get; set; }

        public IDictionary<string, dynamic> RouteDictionary { get; set; }

        public RestRoute()
        {
            CreateRequest = new RestRouteFluentInterface(this);
        }
    }
}