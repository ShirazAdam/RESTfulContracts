using System.Threading.Tasks;

namespace RESTfulContract.Service
{
    public interface IRestfulSample
    {
        Task<string> GetSample(int id);

        Task<string> GetSampleUsingFluentInterface(int id);
    }
}